package d25_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestD25(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "D25 Suite")
}
