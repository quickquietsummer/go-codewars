package d25

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func dotest(walk []rune, expected bool) {
	Expect(IsValidWalk(walk)).To(Equal(expected), "With walk = %s", string(walk))
}

var _ = Describe("Tests", func() {
	It("Sample tests", func() {
		dotest([]rune{'n', 's', 'n', 's', 'n', 's', 'n', 's', 'n', 's'}, true)
		dotest([]rune{'w', 'e', 'w', 'e', 'w', 'e', 'w', 'e', 'w', 'e', 'w', 'e'}, false)
		dotest([]rune{'w'}, false)
		dotest([]rune{'n', 'n', 'n', 's', 'n', 's', 'n', 's', 'n', 's'}, false)
		dotest([]rune{'e', 'e', 'e', 'e', 'w', 'w', 's', 's', 's', 's'}, false)
	})
})
